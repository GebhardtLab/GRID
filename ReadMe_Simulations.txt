Commadline acess to simulation scripts

Preparation:
In Matlab mark the folder "AutomatizationMethods" right click and select "add folder to path" -> "selectect folders"

Execution:
1) call the benchmark functions with benchmark(mode) where mode is a number choosing a simulation
2) view the meaning of different numbers by typing "help benchmark" to the command line

%1: Two peaks where the slow peak is varied
%2: Two peaks where the fast peak is varied
%3: Broad rate distributions
%4: Variing amplitudes
%5: variing number of peaks
%6: powerlaw
%7: Sample survival histogram

3) Execute the simulation by typing e.g. benchmark(5) for simulating a dataset for Figure 2c
4) plot the result by typing figureplotter(5,0). Save the figure as .png by typing figureplotter(5,1)
